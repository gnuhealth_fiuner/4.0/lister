# -*- coding: utf-8 -*-
##############################################################################
#
#    Thymbra
#    Copyright (C) 2011-2012  Ignacio E. Parszyk <iparszyk@thymbra.com>
#                             Sebastián Marró <smarro@thymbra.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.pool import Pool
from .account import *
from .health import *
from .purchase import *
from .stock import *


def register():
    Pool.register(
        AccountPartida,
        AccountInputPartida,
        AccountEgressOrder,
        AccountIssuedCheck,
        Party,
        SocialPlans,
        PatientSocialPlans,
        PatientData,
        AppointmentReport,
        GnuHealthPatientLabTest,
        TestType,
        PatientPregnancy,
        Surgery,
        PatientSurgeryLineMedicament,
        PatientSurgeryLineInput,
        HealthProfessional,
        ChildhoodStage,
        ChildhoodTest,
        PatientEvaluation,
        PatientEvaluationChildhoodInfantTest,
        PatientEvaluationChildhoodEarlyTest,
        Medicament,
        PatientPrescriptionOrder,
        PrescriptionLine,
        InpatientRegistration,
        InpatientMedication,
        ImagingTestResult,
        PaperArchive,
        PrintMedicationDeliveredDayStart,
        PrintMedicationDeliveredProductStart,
        Move,
        Configuration,
        Location,
        ProductLocationStock,
        ProductLocationRel,
        Product,
        AppointmentsSpecialty,
        OpenAppointmentsSpecialtyStart,
        PracticesReport,
        OpenPracticesReportStart,
        HospitalizationReport,
        OpenHospitalizationReportStart,
        PrintPatientEvaluationReportStart,
        module='Lister', type_='model')
    Pool.register(
        PrintMedicationDeliveredDay,
        PrintMedicationDeliveredProduct,
        OpenAppointmentsSpecialty,
        OpenPracticesReport,
        OpenHospitalizationReport,
        OpenInpatientEvaluations,
        PrintPatientEvaluationReport,
        module='Lister', type_='wizard')
    Pool.register(
        AppointmentsReportEvaluation,
        PatientEvaluationReport,
        MedicationDeliveredDay,
        MedicationDeliveredProduct,
        module='Lister', type_='report')
